import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import { withRouter, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import AppFrame from './../components/AppFrame';
import CustomerEdit from './../components/CustomerEdit';
import CustomerData from './../components/CustomerData';
import { getCustomerByDni } from './../selectors/customers';
import { fetchCustomers } from './../actions/fetchCustomers';
import { updateCustomer } from './../actions/updateCustomer';
import { deleteCustomer } from './../actions/deleteCustomer';

class CustomerContainer extends Component {

    componentDidMount(){
        if(!this.props.customer){
            this.props.fetchCustomers();
        }
    }

    handleSubmit = values => {
        const { id } = values;
        return this.props.updateCustomer(id, values).then( r => {
            if(r.payload && r.payload.error){
                throw new SubmissionError(r.payload.error);
            }
        });
    }

    handleOnDelete = id => {
        this.props.deleteCustomer(id).then( v => {
            this.props.history.goBack();
        });
    }

    handleOnBack = () =>{
        this.props.history.goBack();
    }

    handleSubmitSuccess = () => {
        this.props.history.goBack();
    }
    
    renderCustomerControl = (isEdit, isDelete) => {
        if(this.props.customer){
            const CustomerControl = isEdit ? CustomerEdit : CustomerData;
            return <CustomerControl 
                    {...this.props.customer} 
                    onSubmit={this.handleSubmit} 
                    onSubmitSuccess={this.handleSubmitSuccess}
                    onBack={this.handleOnBack}
                    isDeleteAllow={!!isDelete}
                    onDelete={this.handleOnDelete}
                />
        }
        return null;
    }
    renderBody = () => (
        <Route path="/customers/:dni/edit" children={
            ({match: isEdit}) => (
                <Route path="/customers/:dni/delete" children={
                    ({match: isDelete}) => (this.renderCustomerControl(isEdit, isDelete))
                } /> )
        } />
    );
    render() {
        return (
            <div>
                <AppFrame header={`Cliente ${this.props.dni}`}
                body={this.renderBody()} />
            </div>
        );
    }
}

CustomerContainer.propTypes = {
    dni: PropTypes.string.isRequired,
    customer: PropTypes.object,
    fetchCustomers: PropTypes.func.isRequired,
    updateCustomer: PropTypes.func.isRequired,
    deleteCustomer: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
    customer: getCustomerByDni(state, props)
});

export default withRouter(connect(mapStateToProps, {fetchCustomers, updateCustomer, deleteCustomer})(CustomerContainer));