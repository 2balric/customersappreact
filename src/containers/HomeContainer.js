import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import AppFrame from './../components/AppFrame';
import CustomersActions from './../components/CustomersActions';

class HomeContainer extends Component {

    handleOnClick = () =>{
        console.log("handle on lcick");
        this.props.history.push("/customers")
    }
    render() {
        return (
            <div>
                <AppFrame header="Home" body={
                    <div>Esta es la pantalla inisial
                        <CustomersActions>
                            <button onClick={this.handleOnClick}>Listado de clientes</button>
                        </CustomersActions>
                    </div>   
                }></AppFrame>
            </div>
        );
    }
}

HomeContainer.propTypes = {

};

export default withRouter(HomeContainer);